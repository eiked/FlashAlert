package fr.jnda.android.flashalert.ui

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import android.widget.RemoteViews
import androidx.core.content.edit
import fr.jnda.android.flashalert.R



/**
 * Implementation of App Widget functionality.
 */
const val ACTION_WIDGET = "fr.jnda.android.flashalert.WIDGET"

class Widget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {

        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {}

    override fun onDisabled(context: Context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit {
            putBoolean("isActivate", true)
        }
    }


    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        if (ACTION_WIDGET == intent.action) {
            if (!intent.hasExtra("state")) {
                val preferenceManager = PreferenceManager.getDefaultSharedPreferences(context)
                preferenceManager.edit {
                    putBoolean("isActivate", !preferenceManager.getBoolean("isActivate", true))
                }
            }
            val views = RemoteViews(context.packageName, R.layout.widget)
            val isEnabled = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("isActivate",true)

            if (isEnabled) {
                views.setInt(R.id.layout_status, "setBackgroundResource", R.color.widget_text_on)

            }
            else {
                views.setInt(R.id.layout_status, "setBackgroundResource", R.color.widget_text_off)
            }
            views.setOnClickPendingIntent(R.id.imageView, getPendingIntent(context))
            views.setOnClickPendingIntent(R.id.layout_widget, getPendingIntent(context))
            val widgetManager = AppWidgetManager.getInstance(context)
            widgetManager.updateAppWidget(ComponentName(context, Widget::class.java),views)
        }
    }

    companion object {

        internal fun getPendingIntent(context: Context): PendingIntent {
            val intent = Intent(context, Widget::class.java)
            intent.action = ACTION_WIDGET
            return PendingIntent.getBroadcast(context, 0, intent, 0)
        }

        internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager,
                                     appWidgetId: Int) {

            val views = RemoteViews(context.packageName, R.layout.widget)
            views.setOnClickPendingIntent(R.id.imageView, getPendingIntent(context))
            views.setOnClickPendingIntent(R.id.layout_widget, getPendingIntent(context))
            val isEnabled = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("isActivate",true)

            if (isEnabled) {
                views.setInt(R.id.layout_status, "setBackgroundResource", R.color.widget_text_on)
            }
            else {
                views.setInt(R.id.layout_status, "setBackgroundResource", R.color.widget_text_off)
            }
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

