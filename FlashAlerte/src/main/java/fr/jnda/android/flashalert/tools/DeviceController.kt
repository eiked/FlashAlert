package fr.jnda.android.flashalert.tools

import android.content.Context
import android.content.pm.PackageManager
import android.os.PowerManager
import android.preference.PreferenceManager
import android.provider.Settings
import fr.jnda.android.flashalert.FlashAlert

object DeviceController {

    private const val zenMode = "zen_mode"
    private const val zenModeOff = 0

    fun continueEvent(context: Context): Boolean{
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        // Controle si le device a une camera avec flash
        val hasFlash = context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
        if (!hasFlash)
            return false

        // Controle si l'application est active ou non
        val isActive = sharedPreferences.getBoolean("isActivate",true)

        if (!isActive)
            return false

        val onScreeOn = sharedPreferences.getBoolean("event_screenon",false)
        val isScreenOn = isScreenOn(context)
        if (onScreeOn && isScreenOn && !FlashAlert.isRunning)
            return false

        val dnd = getDNDMode(context)
        // 0 - If DnD is off.
        // 1 - If DnD is on -RenderScript.Priority Only
        // 2 - If DnD is on - Total Silence
        // 3 - If DnD is on - Alarms Only
        if (dnd > 0 && !sharedPreferences.getBoolean("event_dnd", false) && !FlashAlert.isRunning){
            return false
        }
        return true
    }

    private fun isScreenOn(context: Context): Boolean {
        val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        return pm.isInteractive
    }

    private fun getDNDMode(context: Context): Int{
        val resolver = context.contentResolver
        return try {
            Settings.Global.getInt(resolver, zenMode)
        } catch (e: Settings.SettingNotFoundException) {
            zenModeOff
        }
    }

}